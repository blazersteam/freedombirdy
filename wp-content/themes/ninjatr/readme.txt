/*-----------------------------------------------------------------------------------*/
/* Fincorp WordPress Theme */
/*-----------------------------------------------------------------------------------*/

Theme Name      :   Fincorp
Theme URI       :   https://flythemes.net/wordpress-themes/fincorp-free-wordpress-theme/
Version         :   1.1.1
Tested up to    :   WP 4.8.2
Author          :   Flythemes
Author URI      :   https://www.flythemes.net/

license         :   GNU General Public License v3.0
License URI     :   http://www.gnu.org/licenses/gpl.html

/*-----------------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*-----------------------------------------------------------------------------------*/

email       :   support@flythemes.net

/*-----------------------------------------------------------------------------------*/
/* Theme Resources */
/*-----------------------------------------------------------------------------------*/

Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.
- jquery.nivo.slider.js is licensed under MIT. 


2 - PT Roboto Condensed - https://www.google.com/fonts/specimen/roboto+condensed
	License: Distributed under the terms of the Apache License, version 2.0 http://www.apache.org/licenses/LICENSE-2.0.html

4 - Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

5 - 	Images used from Pixabay.
	Pixabay provides images under CC0 license 
	(https://creativecommons.org/about/cc0)	

	Slider:
	------
	https://pixabay.com/en/girl-beauty-studio-seductive-1252994/

/*-----------------------------------------------------------------------------------*/
/* Copyright */
/*-----------------------------------------------------------------------------------*/

Fincorp WordPress Theme, Copyright 2017
Fincorp is distributed under the terms of the GNU GPL

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see < http://www.gnu.org/licenses/ >
        
       
Fincorp Pro version
==========================================================
Fincorp pro version is compatible with GPL licensed.


For any help you can mail us at support[at]flythemes.net