<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_MEMORY_LIMIT', '256M');

// ** MySQL settings - You can get this info from your web host ** //


/** The name of the database for WordPress */
define( 'DB_NAME', 'freedomb_database' );

/** MySQL database username */
define( 'DB_USER', 'freedomb_admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Flg(Ll.Y$_1F' );

/** DB Write Permission */
define('FS_METHOD','direct');

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
/*
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );
*/
define('AUTH_KEY',         '@7HI<heAmX~|c)m+{4Eu?q|P+y5?a_FIJ!q-f.nV%h??uX=t9FptYQCjiE=zI&bw');
define('SECURE_AUTH_KEY',  'k@(`emyAD~C_QW+5Fl8%8S[ K4o_-rg-L^.[u|2--*Lk?%RR5NR 1~qN20 rA`4s');
define('LOGGED_IN_KEY',    '.U-}N9hVN,1/0guPdVd`{H+.^E s-sQFNXQf^EX 7ZgGTp]%r f$+/[i!793E?Um');
define('NONCE_KEY',        'ZHa[<v0orvYh^tH|/L5:[zylCxG&FlixBt9.a aL&/i) F+z<k7h?V9agA]?6F?`');
define('AUTH_SALT',        'RG wKG@~!s0uG$iI}~6OVuO|S%pL<u@bAeBgAA7`M[-p49%ya4I7o]d<f*:3{g-5');
define('SECURE_AUTH_SALT', '[;2T~S{8f,`-p)8@dAe^N4&^Agy2k{a&:Arj~@if3n~t.lr`@l?Kye;wA:XTP%My');
define('LOGGED_IN_SALT',   'z&E D!RthifOff06FtovXatE&&-R/uTeSacxNd/|RO5ZoK$#>QI)hn6*w.+2gx5e');
define('NONCE_SALT',       'SaHQIXCcDki8T)WyGQ-;^}Zkn^=1qw1E8[0}}lXTQXK7wcqzz(wG|e zR~@0OOk?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'dome_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
